Step to run:

- Configure your mysql database and put in .env with .env_example guiding

Run commands:

- npm install

- npm run db:migrate

- npm run db:seed

- npm run sync-article-data

- npm run dev

