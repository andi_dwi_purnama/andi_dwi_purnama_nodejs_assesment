require('dotenv').config();

const axios = require('axios');

const NYT_BASE_API_URL = process.env.NYT_API_URL;
const NYT_API_KEY = process.env.NYT_API_KEY;

const searchArticle = async ({query,sort,page=0} ) => {

  let fetch_search_artcile = {};

  if (query) {
    fetch_search_artcile = `articlesearch.json?q=${query}&sort=${sort}&page=${page}&api-key=${NYT_API_KEY}`;
  } else {
    fetch_search_artcile = `articlesearch.json?sort=${sort}&page=${page}&api-key=${NYT_API_KEY}`;
  }
  
  const instance = axios.create({ baseURL: NYT_BASE_API_URL });

  try {
    const { data: response } = await instance.get(fetch_search_artcile);

    return response;

  } catch (error) {
    console.log(error, 'Error in get search article');
    
  }
    
};

const initAxiosInstance = ({ baseURL, timeout }) => axios.create({ baseURL, timeout });

module.exports = {
  searchArticle,
  initAxiosInstance
};