'use strict';
const table_name = 'Books'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(table_name, {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      list_name: {
        type: Sequelize.STRING
      },
      display_name: {
        type: Sequelize.STRING
      },
      bestsellers_date: {
        type: Sequelize.STRING
      },
      published_date: {
        type: Sequelize.STRING
      },
      rank: {
        type: Sequelize.INTEGER
      },
      rank_last_week: {
        type: Sequelize.INTEGER
      },
      weeks_on_list: {
        type: Sequelize.INTEGER
      },
      asterisk: {
        type: Sequelize.INTEGER
      },
      dagger: {
        type: Sequelize.INTEGER
      },
      amazon_product_url: {
        type: Sequelize.STRING
      },
      isbns: {
        type: Sequelize.JSON
      },
      reviews: {
        type: Sequelize.JSON
      },
      book_details: {
        type: Sequelize.JSON
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable(table_name);
  }
};