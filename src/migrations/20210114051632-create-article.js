'use strict';

const table_name = 'Articles';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(table_name, {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      abstract: {
        type: Sequelize.STRING(1000)
      },
      web_url: {
        type: Sequelize.STRING
      },
      snippet: {
        type: Sequelize.STRING
      },
      lead_paragraph: {
        type: Sequelize.STRING(1000)
      },
      source: {
        type: Sequelize.STRING
      },
      multimedia: {
        type: Sequelize.JSON
      },
      headline: {
        type: Sequelize.JSON
      },
      keywords: {
        type: Sequelize.JSON
      },
      pub_date: {
        type: Sequelize.STRING
      },
      document_type: {
        type: Sequelize.STRING
      },
      section_name: {
        type: Sequelize.STRING
      },
      subsection_name: {
        type: Sequelize.STRING
      },
      byline: {
        type: Sequelize.JSON
      },
      type_of_material: {
        type: Sequelize.STRING
      },
      _id: {
        type: Sequelize.STRING
      },
      word_count: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable(table_name);
  }
};