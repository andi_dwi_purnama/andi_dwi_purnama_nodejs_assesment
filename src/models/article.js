'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Article extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Article.init({
    abstract: DataTypes.STRING(1000),
    web_url: {
      type: DataTypes.STRING
    },
    snippet: {
      type: DataTypes.STRING
    },
    lead_paragraph: {
      type: DataTypes.STRING(1000)
    },
    source: {
      type: DataTypes.STRING
    },
    multimedia: {
      type: DataTypes.JSON
    },
    headline: {
      type: DataTypes.JSON
    },
    keywords: {
      type: DataTypes.JSON
    },
    pub_date: {
      type: DataTypes.STRING
    },
    document_type: {
      type: DataTypes.STRING
    },
    section_name: {
      type: DataTypes.STRING
    },
    subsection_name: {
      type: DataTypes.STRING
    },
    byline: {
      type: DataTypes.JSON
    },
    type_of_material: {
      type: DataTypes.STRING
    },
    _id: {
      type: DataTypes.STRING
    },
    word_count: {
      type: DataTypes.INTEGER
    },
  }, {
    sequelize,
    modelName: 'Article',
  });
  return Article;
};