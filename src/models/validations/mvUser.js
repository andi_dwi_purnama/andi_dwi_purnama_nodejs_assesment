const yup = require('yup')

const xyup = require('./xyup')

const dict = {
  id: {
    email: {
      email: 'Email is not valid',
    },
    required: {
      fullName: 'Name is required',
      email: 'Email is required',
      phone: 'Phone is required',
      newPassword: 'Password is required',
      confirmNewPassword: 'Confirm Password is required',
      RoleId: 'Role is required',
    },
  },
}

const getShapeSchema = (required, language) => {
  // Default Langauge Id (Indonesia)
  const msg = Object.assign(dict.id, dict[language])
  return {
    id: xyup.uuid('Invalid Id', required),
    fullName: yup.string().required(msg.required.fullName),
    email: yup.string().email(msg.email.email).required(msg.required.email),
    phone: yup.string().required(msg.required.phone),
    active: yup.string().nullable(),
    tokenVerify: yup.string().nullable(),
    newPassword: yup
      .string()
      .min(8, 'Min 8 character')
      .oneOf([yup.ref('confirmNewPassword')], 'The Password is not same'),
    confirmNewPassword: yup
      .string()
      .min(8, 'Min 8 character')
      .oneOf([yup.ref('newPassword')], 'The Password is not same'),
    RoleId: yup.string().required(msg.required.RoleId),
  }
}

module.exports = xyup.generateFormSchema(getShapeSchema)
