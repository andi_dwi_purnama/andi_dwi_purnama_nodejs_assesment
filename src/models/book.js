'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Book extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Book.init({
    list_name: DataTypes.STRING,
    display_name: {
      type: DataTypes.STRING
    },
    bestsellers_date: {
      type: DataTypes.STRING
    },
    published_date: {
      type: DataTypes.STRING
    },
    rank: {
      type: DataTypes.INTEGER
    },
    rank_last_week: {
      type: DataTypes.INTEGER
    },
    weeks_on_list: {
      type: DataTypes.INTEGER
    },
    asterisk: {
      type: DataTypes.INTEGER
    },
    dagger: {
      type: DataTypes.INTEGER
    },
    amazon_product_url: {
      type: DataTypes.STRING
    },
    isbns: {
      type: DataTypes.JSON
    },
    reviews: {
      type: DataTypes.JSON
    },
    book_details: {
      type: DataTypes.JSON
    },
  }, {
    sequelize,
    modelName: 'Book',
  });
  return Book;
};