import express from 'express'
import { Router as UnoRouter } from 'uno-api'
import { wrapperRequest } from 'helpers/ExpressHelpers'

/* Setup Router */
const router = express.Router()
const apiPublic = new UnoRouter(router, {
  wrapperRequest,
})

/* Declare Controller */
const AuthController = require('controllers/AuthController')
const RoleController = require('controllers/RoleController')
const UserController = require('controllers/UserController')
const ArticleController = require('controllers/ArticleController')

/* Authentication */
apiPublic.create({
  baseURL: '/auth',
  postWithParam: [
    ['signup', AuthController.signUp],
    ['signin', AuthController.signIn],
  ],
})

/* User */
apiPublic.create({
  baseURL: '/user',
  get: UserController.getAll,
  getWithParam: [[':id', UserController.getOne]],
})

/* Role */
apiPublic.create({
  baseURL: '/role',
  get: RoleController.getAll,
  getWithParam: [[':id', RoleController.getOne]],
})

/* Article */
router.get('/articles', ArticleController.list);
router.get('/articles/:id', ArticleController.detail);

module.exports = router
