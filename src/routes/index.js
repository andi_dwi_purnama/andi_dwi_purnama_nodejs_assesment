import express from 'express'
import PublicRoute from './public'
import PrivateRoute from './private' // with middleware

const router = express.Router()

/* Home Page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Express',
    description: 'Homepage',
  })
})

/* Forbidden Page. */
router.get('/v1', function (req, res, next) {
  res.render('index', {
    title: 'Forbidden Access',
    description: 'Forbidden Access',
    code: '403',
  })
})

router.use('/v1', PublicRoute)
router.use('/v1', PrivateRoute)

/* Not Found Page. */
router.get('*', function (req, res, next) {
  res.render('index', {
    title: 'Oops, Page not found!',
    description: 'Not Found',
    code: '404',
  })
})

module.exports = router
