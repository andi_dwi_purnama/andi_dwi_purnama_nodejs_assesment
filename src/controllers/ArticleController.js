const {
  Article
} = require('../models');

const Sequelize = require('sequelize');

const Op = Sequelize.Op;

const list = async (req, res) => {
  const {
    limit,
    offset,
    keyword = '',
    sort
  } = req.query;

  let order = [];

  if (sort == 'oldest') {
    order.push([
      'pub_date', 'ASC'
    ])
  }

  if (sort == 'newest') {
    order.push([
      'pub_date', 'DESC'
    ])
  }

  try {
    const getArticles = await Article.findAll({
      where: {
        [Op.or]: [{
            abstract: {
              [Op.like]: `%${keyword}%`
            }
          },
          {
            lead_paragraph: {
              [Op.like]: `%${keyword}%`
            }
          },
          {
            snippet: {
              [Op.like]: `%${keyword}%`
            }
          }
        ]
      },
      order
    });

    return res.status(200).send({
      message: getArticles,
    });
  } catch (error) {
    return res.status(400).send({
      error: error,
      message: 'Error occured',
    });
  }
}

const detail = async (req, res) => {
  try {
    const getArticle = await Article.findOne({
        where: {
            id: req.params.id
        }
    });

    return res.status(200).send({
      message: getArticle,
    });
  } catch (error) {
    return res.status(400).send({
      error: error,
      message: 'Error occured',
    });
  }
}

module.exports = {
  list,
  detail
}
