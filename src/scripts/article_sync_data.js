const { searchArticle } = require('../helpers/NytApiCaller')

const { Article } = require('../models');

const nyt_default_item = 10;

const main = async () => {
  // Asumption
  // We just get 5000 article from NYT and save to database

  console.log('Inputing article data...');

  for (let index = 0; 5000/nyt_default_item; index++) {
    const articles = await searchArticle({sort: 'oldest', page: index});

    // Save article data to database
    await Article.bulkCreate(articles.response.docs);
  }

  console.log('Input article data finished!');

}

main()
